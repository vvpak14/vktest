// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'photo_size.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PhotoSize _$PhotoSizeFromJson(Map<String, dynamic> json) {
  return PhotoSize(
    json['url'] as String,
    json['width'] as int,
    json['height'] as int,
    _$enumDecode(_$PhotoSizeTypeEnumMap, json['type']),
  );
}

Map<String, dynamic> _$PhotoSizeToJson(PhotoSize instance) => <String, dynamic>{
      'url': instance.url,
      'width': instance.width,
      'height': instance.height,
      'type': _$PhotoSizeTypeEnumMap[instance.type],
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

const _$PhotoSizeTypeEnumMap = {
  PhotoSizeType.s: 's',
  PhotoSizeType.m: 'm',
  PhotoSizeType.x: 'x',
  PhotoSizeType.o: 'o',
  PhotoSizeType.p: 'p',
  PhotoSizeType.q: 'q',
  PhotoSizeType.r: 'r',
  PhotoSizeType.y: 'y',
  PhotoSizeType.z: 'z',
  PhotoSizeType.w: 'w',
};
