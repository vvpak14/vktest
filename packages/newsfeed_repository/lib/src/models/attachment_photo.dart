import 'package:json_annotation/json_annotation.dart';
import 'package:newsfeed_repository/src/models/attachment_data.dart';
import 'package:newsfeed_repository/src/models/photo_size.dart';

part 'attachment_photo.g.dart';

@JsonSerializable(
  fieldRename: FieldRename.snake,
)
class AttachmentPhoto extends AttachmentData {
  final int id;
  final int albumId;
  final int ownerId;
  final int userId;
  final String text;
  final int date;
  final List<PhotoSize> sizes;

  AttachmentPhoto(
    this.id,
    this.albumId,
    this.ownerId,
    this.userId,
    this.text,
    this.date,
    this.sizes,
  );

  factory AttachmentPhoto.fromJson(Map<String, dynamic> json) =>
      _$AttachmentPhotoFromJson(json);

  @override
  List<Object?> get props => [
        id,
        albumId,
        ownerId,
        userId,
        text,
        date,
        sizes,
      ];
}
