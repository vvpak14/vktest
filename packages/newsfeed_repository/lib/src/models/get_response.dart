import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:newsfeed_repository/src/models/news_item.dart';

part 'get_response.g.dart';

@JsonSerializable(
  fieldRename: FieldRename.snake,
)
class GetResponse extends Equatable {
  GetResponse(this.items);

  final List<NewsItem> items;

  factory GetResponse.fromJson(Map<String, dynamic> json) =>
      _$GetResponseFromJson(json['response']);

  @override
  List<Object?> get props => [items];
}
