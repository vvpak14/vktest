// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewsItem _$NewsItemFromJson(Map<String, dynamic> json) {
  return NewsItem(
    _$enumDecode(_$NewsTypeEnumMap, json['type']),
    json['source_id'] as int?,
    json['post_id'] as int?,
    json['text'] as String?,
    (json['attachments'] as List<dynamic>?)
        ?.map((e) => Attachment.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$NewsItemToJson(NewsItem instance) => <String, dynamic>{
      'type': _$NewsTypeEnumMap[instance.type],
      'source_id': instance.sourceId,
      'post_id': instance.postId,
      'text': instance.text,
      'attachments': instance.attachments,
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

const _$NewsTypeEnumMap = {
  NewsType.post: 'post',
  NewsType.photo: 'photo',
  NewsType.photo_tag: 'photo_tag',
  NewsType.wall_photo: 'wall_photo',
  NewsType.friend: 'friend',
  NewsType.note: 'note',
  NewsType.audio: 'audio',
  NewsType.video: 'video',
};
