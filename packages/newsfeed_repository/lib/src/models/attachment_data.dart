import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
abstract class AttachmentData extends Equatable {
  factory AttachmentData.fromJson(Map<String, dynamic> json) =>
      throw Exception('Unsupported type');

  AttachmentData();

  @override
  List<Object?> get props => [];
}
