// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetResponse _$GetResponseFromJson(Map<String, dynamic> json) {
  return GetResponse(
    (json['items'] as List<dynamic>)
        .map((e) => NewsItem.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$GetResponseToJson(GetResponse instance) =>
    <String, dynamic>{
      'items': instance.items,
    };
