import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:newsfeed_repository/newsfeed_repository.dart';

part 'news_item.g.dart';

enum NewsType {
  post,
  photo,
  photo_tag,
  wall_photo,
  friend,
  note,
  audio,
  video,
}

@JsonSerializable(
  fieldRename: FieldRename.snake,
)
class NewsItem extends Equatable {
  final NewsType type;
  final int? sourceId;
  final int? postId;
  final String? text;
  final List<Attachment>? attachments;

  NewsItem(
    this.type,
    this.sourceId,
    this.postId,
    this.text,
    this.attachments,
  );

  factory NewsItem.fromJson(Map<String, dynamic> json) =>
      _$NewsItemFromJson(json);

  @override
  List<Object?> get props => [
        type,
        sourceId,
        postId,
        text,
      ];
}
