import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:newsfeed_repository/newsfeed_repository.dart';

part 'attachment.g.dart';

enum AttachmentType { photo, link }

@JsonSerializable(
  fieldRename: FieldRename.snake,
)
class Attachment extends Equatable {
  Attachment(this.type, this.photo);

  final AttachmentType type;
  final AttachmentPhoto? photo;

  factory Attachment.fromJson(Map<String, dynamic> json) =>
      _$AttachmentFromJson(json);

  @override
  List<Object?> get props => [type, photo];
}
