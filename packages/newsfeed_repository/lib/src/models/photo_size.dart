import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'photo_size.g.dart';

enum PhotoSizeType {
  s,
  m,
  x,
  o,
  p,
  q,
  r,
  y,
  z,
  w,
}

@JsonSerializable(
  fieldRename: FieldRename.snake,
)
class PhotoSize extends Equatable {
  final String url;
  final int width;
  final int height;
  final PhotoSizeType type;

  PhotoSize(
    this.url,
    this.width,
    this.height,
    this.type,
  );

  factory PhotoSize.fromJson(Map<String, dynamic> json) =>
      _$PhotoSizeFromJson(json);
  Map<String, dynamic> toJson() => _$PhotoSizeToJson(this);

  @override
  List<Object?> get props => [
        url,
        width,
        height,
        type,
      ];
}
