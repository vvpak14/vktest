// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'attachment_photo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AttachmentPhoto _$AttachmentPhotoFromJson(Map<String, dynamic> json) {
  return AttachmentPhoto(
    json['id'] as int,
    json['album_id'] as int,
    json['owner_id'] as int,
    json['user_id'] as int,
    json['text'] as String,
    json['date'] as int,
    (json['sizes'] as List<dynamic>)
        .map((e) => PhotoSize.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$AttachmentPhotoToJson(AttachmentPhoto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'album_id': instance.albumId,
      'owner_id': instance.ownerId,
      'user_id': instance.userId,
      'text': instance.text,
      'date': instance.date,
      'sizes': instance.sizes,
    };
