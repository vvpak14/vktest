import 'package:local_storage_repository/local_storage_repository.dart';
import 'package:newsfeed_repository/src/clients/newsfeed_client.dart';
import 'package:newsfeed_repository/src/models/get_response.dart';
import 'package:newsfeed_repository/src/models/news_item.dart';

abstract class INewsfeedRepository {
  Future<List<NewsItem>> getNewsList();
}

class NewsfeedRepository implements INewsfeedRepository {
  final INewsFeedClient _client;
  final LocalStorageRepository _localStorageRepository;

  NewsfeedRepository(INewsFeedClient client)
      : _client = client,
        _localStorageRepository = LocalStorageRepository();

  Future<List<NewsItem>> getNewsList() async {
    final accessToken = await _localStorageRepository.getItem('token');
    final json = await _client.get(accessToken: accessToken!);
    final result = GetResponse.fromJson(json).items;
    return result;
  }
}
