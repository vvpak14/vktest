import 'package:dio/dio.dart';

abstract class INewsFeedClient {
  Future<Map<String, dynamic>> get({
    required String accessToken,
    int count = 50,
    int startFrom = 0,
  });
}

class NewsFeedClient implements INewsFeedClient {
  @override
  Future<Map<String, dynamic>> get({
    required String accessToken,
    int count = 50,
    int startFrom = 0,
  }) async {
    final baseUrl = 'https://api.vk.com/method/newsfeed.get?v=5.77';
    var response = await Dio().get('$baseUrl&access_token=$accessToken');

    if (response.statusCode != 200) {
      throw Exception('Get newsfeed error');
    }

    return response.data;
  }
}
