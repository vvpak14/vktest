import 'package:local_storage_repository/local_storage_repository.dart';
import 'package:test/test.dart';

void main() {
  //Does not work without flutter sdk
  test('', () async {
    var storage = LocalStorageRepository();
    await storage.setItem('test', 'test');
    var res = storage.getItem('test');
    expect(res, 'test');
  });
}
