import 'package:local_storage_repository/src/local_storage_repository.dart';

LocalStorageRepository getLocalStorageRepository() => throw UnsupportedError(
      'Cannot create LocalStorageRepository for this platform',
    );
