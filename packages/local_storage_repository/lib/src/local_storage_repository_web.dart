import 'package:local_storage_repository/src/local_storage_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageRepositoryWeb implements LocalStorageRepository {
  late SharedPreferences _storage;

  LocalStorageRepository() {
    init();
  }

  Future<void> init() async {
    _storage = await SharedPreferences.getInstance();
  }

  @override
  Future<String?> getItem(String key) async {
    return Future.value(_storage.get(key).toString());
  }

  @override
  Future<void> setItem(String key, String value) async {
    await _storage.setString(key, value);
  }

  @override
  Future<void> deleteItem(String key) async {
    await _storage.remove(key);
  }
}

LocalStorageRepository getLocalStorageRepository() =>
    LocalStorageRepositoryWeb();
