import 'local_storage_stub.dart'
    if (dart.library.io) 'local_storage_repository_mobile.dart'
    if (dart.library.html) 'local_storage_repository_web.dart';

abstract class LocalStorageRepository {
  Future<void> setItem(String key, String value);
  Future<String?> getItem(String key);
  Future<void> deleteItem(String key);

  factory LocalStorageRepository() => getLocalStorageRepository();
}
