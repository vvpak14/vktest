import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:local_storage_repository/src/local_storage_repository.dart';

class LocalStorageRepositoryMobile implements LocalStorageRepository {
  final FlutterSecureStorage _storage = FlutterSecureStorage();

  @override
  Future<String?> getItem(String key) async {
    return await _storage.read(key: key);
  }

  @override
  Future<void> setItem(String key, String value) async {
    await _storage.write(key: key, value: value);
  }

  @override
  Future<void> deleteItem(String key) async {
    await _storage.delete(key: key);
  }
}

LocalStorageRepository getLocalStorageRepository() =>
    LocalStorageRepositoryMobile();
