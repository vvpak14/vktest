import 'dart:async';

import 'package:authentication_repository/authentication_repository.dart';
import 'package:authentication_repository/src/clients/authentication_client.dart';
import 'package:local_storage_repository/local_storage_repository.dart';

enum AuthenticationStatus {
  unknown,
  authenticated,
  unauthenticated,
}

abstract class IAuthenticationRepository {
  Stream<AuthenticationStatus> get status;
  String get userToken;
  void dispose();
}

class AuthenticationRepository implements IAuthenticationRepository {
  final IAuthenticationClient _client;
  final LocalStorageRepository _localStorageRepository;
  final _controller = StreamController<AuthenticationStatus>();

  String? _userToken;

  String get userToken => _userToken ?? '';

  AuthenticationRepository(IAuthenticationClient client)
      : _client = client,
        _localStorageRepository = LocalStorageRepository();

  Stream<AuthenticationStatus> get status async* {
    yield AuthenticationStatus.unauthenticated;
    yield* _controller.stream;
  }

  Future<void> login({
    required String login,
    required String password,
  }) async {
    try {
      final json = await _client.authenticate(login: login, password: password);
      final data = AuthenticationData.fromJson(json);
      _controller.add(AuthenticationStatus.authenticated);
      _userToken = data.accessToken;
      _localStorageRepository.setItem('token', data.accessToken);
    } catch (e) {
      print(e);
    }
  }

  void dispose() => _controller.close();
}
