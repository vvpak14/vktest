import 'package:dio/dio.dart';

abstract class IAuthenticationClient {
  Future<Map<String, dynamic>> authenticate({
    required String login,
    required String password,
  });
}

class AuthenticationClient implements IAuthenticationClient {
  final Dio _dio;

  AuthenticationClient(Dio dio) : _dio = dio;

  @override
  Future<Map<String, dynamic>> authenticate({
    required String login,
    required String password,
  }) async {
    var response = await _dio.get(
      'https://oauth.vk.com/token?grant_type=password&client_id=2274003&client_secret=hHbZxrka2uZ6jB1inYsH&username=${login}&password=${password}&scope=*',
    );

    if (response.statusCode != 200) {
      throw Exception('Authentication error');
    }

    return response.data;
  }
}
