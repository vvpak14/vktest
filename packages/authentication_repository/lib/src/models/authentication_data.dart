import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'authentication_data.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class AuthenticationData extends Equatable {
  final String accessToken;
  final int expiresIn;
  final int userId;

  AuthenticationData({
    required this.accessToken,
    required this.userId,
    required this.expiresIn,
  });

  factory AuthenticationData.fromJson(Map<String, dynamic> json) =>
      _$AuthenticationDataFromJson(json);

  Map<String, dynamic> toJson() => _$AuthenticationDataToJson(this);

  @override
  List<Object?> get props => [accessToken, expiresIn, userId];
}
