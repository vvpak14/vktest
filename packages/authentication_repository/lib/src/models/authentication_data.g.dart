// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authentication_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AuthenticationData _$AuthenticationDataFromJson(Map<String, dynamic> json) {
  return AuthenticationData(
    accessToken: json['access_token'] as String,
    userId: json['user_id'] as int,
    expiresIn: json['expires_in'] as int,
  );
}

Map<String, dynamic> _$AuthenticationDataToJson(AuthenticationData instance) =>
    <String, dynamic>{
      'access_token': instance.accessToken,
      'expires_in': instance.expiresIn,
      'user_id': instance.userId,
    };
