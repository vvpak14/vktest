part of 'connection_bloc.dart';

abstract class ConnectionEvent extends Equatable {
  const ConnectionEvent();
}

class ConnectionStatusChanged extends ConnectionEvent {
  const ConnectionStatusChanged(this.status);

  final ConnectivityResult status;

  @override
  List<Object?> get props => [status];
}
