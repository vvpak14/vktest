import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:equatable/equatable.dart';

part 'connection_event.dart';
part 'connection_state.dart';

class ConnectionBloc extends Bloc<ConnectionEvent, ConnectionState> {
  ConnectionBloc() : super(ConnectionInitial()) {
    _connectivitySubscription =
        Connectivity().onConnectivityChanged.listen(_onConnectivityChanged);
  }

  late StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  Stream<ConnectionState> mapEventToState(
    ConnectionEvent event,
  ) async* {
    if (event is ConnectionStatusChanged) {
      yield _mapConnectionStatusChangedToState(event);
    }
  }

  @override
  Future<void> close() {
    _connectivitySubscription.cancel();
    return super.close();
  }

  Future<void> _onConnectivityChanged(ConnectivityResult event) async {
    add(ConnectionStatusChanged(event));
  }

  ConnectionState _mapConnectionStatusChangedToState(
      ConnectionStatusChanged event) {
    switch (event.status) {
      case ConnectivityResult.mobile:
        return ConnectionMobile();
      case ConnectivityResult.wifi:
        return ConnectionWifi();
      case ConnectivityResult.none:
        return ConnectionNone();
    }
  }
}
