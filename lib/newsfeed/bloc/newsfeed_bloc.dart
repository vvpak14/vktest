import 'dart:async';
import 'dart:core';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:newsfeed_repository/newsfeed_repository.dart';

part 'newsfeed_event.dart';
part 'newsfeed_state.dart';

class NewsfeedBloc extends Bloc<NewsfeedEvent, NewsfeedState> {
  NewsfeedBloc({
    required NewsfeedRepository newsfeedRepository,
  })  : _newsfeedRepository = newsfeedRepository,
        super(NewsfeedInitial());

  final NewsfeedRepository _newsfeedRepository;

  @override
  Stream<NewsfeedState> mapEventToState(
    NewsfeedEvent event,
  ) async* {
    if (event is NewsfeedFetched) {
      yield await _mapNewsfeedFetchedToState(state);
    }
  }

  Future<NewsfeedState> _mapNewsfeedFetchedToState(NewsfeedState state) async {
    if (state is NewsfeedInitial) {
      try {
        final news = await _newsfeedRepository.getNewsList();
        return NewsfeedSuccess(news: news);
      } catch (e) {
        return NewsfeedFailure(news: state.news, error: e.toString());
      }
    }

    return state;
  }
}
