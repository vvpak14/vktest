part of 'newsfeed_bloc.dart';

class NewsfeedState extends Equatable {
  const NewsfeedState({
    required this.news,
  }) : super();

  final List<NewsItem> news;

  @override
  List<Object?> get props => [news];
}

class NewsfeedInitial extends NewsfeedState {
  NewsfeedInitial() : super(news: []);
}

class NewsfeedSuccess extends NewsfeedState {
  const NewsfeedSuccess({
    required List<NewsItem> news,
  }) : super(news: news);
}

class NewsfeedFailure extends NewsfeedState {
  const NewsfeedFailure({
    required List<NewsItem> news,
    required this.error,
  }) : super(news: news);

  final String error;

  @override
  List<Object?> get props => [news, error];
}
