import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:newsfeed_repository/newsfeed_repository.dart';
import 'package:vktest/l10n/l10n.dart';
import 'package:vktest/newsfeed/bloc/newsfeed_bloc.dart';
import 'package:vktest/newsfeed/view/news_list.dart';

class NewsfeedPage extends StatelessWidget {
  const NewsfeedPage({Key? key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute(builder: (_) => const NewsfeedPage());
  }

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    return Scaffold(
        appBar: AppBar(
          title: Text(l10n.homePage),
        ),
        body: BlocProvider(
          create: (_) => NewsfeedBloc(
            newsfeedRepository:
                RepositoryProvider.of<NewsfeedRepository>(context),
          )..add(NewsfeedFetched()),
          child: const NewsList(),
        ));
  }
}
