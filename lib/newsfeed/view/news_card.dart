import 'package:flutter/material.dart';

class NewsCard extends StatelessWidget {
  const NewsCard({Key? key, this.imageUrl, this.text}) : super(key: key);

  final String? imageUrl;
  final String? text;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          if (imageUrl != null) Image.network(imageUrl!),
          Text(text ?? ''),
        ],
      ),
    );
  }
}
