import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:vktest/newsfeed/bloc/newsfeed_bloc.dart';
import 'package:vktest/newsfeed/view/news_card.dart';

class NewsList extends StatefulWidget {
  const NewsList({Key? key}) : super(key: key);

  @override
  _NewsListState createState() => _NewsListState();
}

class _NewsListState extends State<NewsList> {
  final _scrollController = ScrollController();
  late NewsfeedBloc _newsfeedBloc;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {});
    _newsfeedBloc = context.read<NewsfeedBloc>();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {}

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewsfeedBloc, NewsfeedState>(
      builder: (context, state) {
        if (state is NewsfeedFailure) {
          return const Center(
            child: Text('FetchingError'),
          );
        }

        if (state is NewsfeedSuccess) {
          return ListView.builder(
            itemBuilder: (context, index) {
              final element = state.news.elementAt(index);
              var imageUrl;
              if (element.attachments != null) {
                if (element.attachments!.first.photo != null) {
                  imageUrl = element.attachments!.first.photo!.sizes.first.url;
                }
              }
              return NewsCard(
                imageUrl: imageUrl,
                text: element.text,
              );
            },
            itemCount: state.news.length,
            controller: _scrollController,
          );
        }

        return const Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }
}
