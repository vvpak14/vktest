part of 'authentication_bloc.dart';

class AuthenticationState extends Equatable {
  const AuthenticationState._({
    this.status = AuthenticationStatus.unknown,
  });

  factory AuthenticationState.unknown() => const AuthenticationState._();

  factory AuthenticationState.authenticated() =>
      const AuthenticationState._(status: AuthenticationStatus.authenticated);

  factory AuthenticationState.unauthenticated() =>
      const AuthenticationState._(status: AuthenticationStatus.unauthenticated);

  final AuthenticationStatus status;

  @override
  List<Object?> get props => [status];
}
