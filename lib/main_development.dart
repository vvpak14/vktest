// Copyright (c) 2021, Very Good Ventures
// https://verygood.ventures
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

import 'dart:async';
import 'dart:developer';

import 'package:authentication_repository/authentication_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:newsfeed_repository/newsfeed_repository.dart';
import 'package:vktest/app/app.dart';
import 'package:vktest/app/app_bloc_observer.dart';

void main() {
  // TODO: change this to DI

  inject();

  runZonedGuarded(
    () => runApp(App()),
    (error, stackTrace) => log(error.toString(), stackTrace: stackTrace),
  );

  Bloc.observer = AppBlocObserver();
  FlutterError.onError = (details) {
    log(details.exceptionAsString(), stackTrace: details.stack);
  };
}

void inject() {
  final dio = Dio();
  dio.options.baseUrl = 'https://api.vk.com';
  final authClient = AuthenticationClient(dio);
  final newsClient = NewsFeedClient();
  GetIt.I.registerSingleton<Dio>(dio);
  GetIt.I.registerSingleton<AuthenticationClient>(authClient);
  GetIt.I.registerSingleton<NewsFeedClient>(newsClient);
}
