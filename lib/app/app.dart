import 'package:authentication_repository/authentication_repository.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get_it/get_it.dart';
import 'package:newsfeed_repository/newsfeed_repository.dart';
import 'package:vktest/authentication/bloc/authentication_bloc.dart';
import 'package:vktest/l10n/l10n.dart';
import 'package:vktest/login/view/login_page.dart';
import 'package:vktest/newsfeed/view/newsfeed_page.dart';
import 'package:vktest/splash/view/splash_page.dart';

class App extends StatefulWidget {
  App({
    Key? key,
  }) : super(key: key) {
    var dio = GetIt.I<Dio>();
    authenticationClient = AuthenticationClient(dio);
    authenticationRepository = AuthenticationRepository(authenticationClient);
  }

  late AuthenticationClient authenticationClient;
  late AuthenticationRepository authenticationRepository;

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final _navigatorKey = GlobalKey<NavigatorState>();

  NavigatorState get _navigator => _navigatorKey.currentState!;

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider.value(
          value: widget.authenticationRepository,
        ),
        RepositoryProvider<NewsfeedRepository>(
          create: (context) =>
              NewsfeedRepository(GetIt.I.get<NewsFeedClient>()),
        )
      ],
      child: BlocProvider(
        create: (_) => AuthenticationBloc(
          authenticationRepository: widget.authenticationRepository,
        ),
        child: _renderAppView(),
      ),
    );
  }

  Widget _renderAppView() => MaterialApp(
        navigatorKey: _navigatorKey,
        localizationsDelegates: const [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
        ],
        supportedLocales: AppLocalizations.supportedLocales,
        builder: (context, child) {
          return BlocListener<AuthenticationBloc, AuthenticationState>(
            listener: (context, state) {
              switch (state.status) {
                case AuthenticationStatus.authenticated:
                  _navigator.pushAndRemoveUntil(
                    NewsfeedPage.route(),
                    (route) => false,
                  );
                  break;
                case AuthenticationStatus.unauthenticated:
                case AuthenticationStatus.unknown:
                  _navigator.pushAndRemoveUntil(
                    LoginPage.route(),
                    (route) => false,
                  );
                  break;
                default:
                  break;
              }
            },
            child: child,
          );
        },
        onGenerateRoute: (_) => SplashPage.route(),
      );
}
